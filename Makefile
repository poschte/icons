SOURCES=$(wildcard *.svg)
NAMES=$(SOURCES:.svg=)
PNGS=$(addsuffix _256.png, ${NAMES}) $(addsuffix _512.png, ${NAMES})
TARGETS=$(addprefix public/, ${PNGS} ${SOURCES})
.PHONY: all clean

test:
	echo ${TARGETS}
	echo ${SOURCES}

all: $(TARGETS)

public/%.svg: %.svg
	cp $^ $@

public/%_512.png: %.svg
	inkscape -w 512 -h 512 -e $@ $^
public/%_256.png: %.svg
	inkscape -w 256 -h 256 -e $@ $^
